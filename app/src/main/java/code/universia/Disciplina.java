package code.universia;

/**
 * Created by Lajos on 10/12/2015.
 */
public class Disciplina {
    private String name;
    private String professor;
    private String criador;
    private String codigo;
    private String periodo;

    public Disciplina(String name, String professor, String criador, String codigo, String periodo){
        this.name = name;
        this.professor = professor;
        this.criador = criador;
        this.codigo = codigo;
        this.periodo = periodo;
    }

    public String getName(){
        return this.name;
    }

    public String getProfessor(){
        return this.professor;
    }

    public String getCriador(){
        return this.criador;
    }

    public String getCodigo(){
        return this.codigo;
    }

    public String getPeriodo(){
        return this.periodo;
    }
}

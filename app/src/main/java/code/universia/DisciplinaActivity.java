package code.universia;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;


public class DisciplinaActivity extends ActionBarActivity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.disciplina_feed);

        // ID DO RECYCLER VIEW EM XML
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerList);


        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);

        recyclerView.setAdapter(new DisciplinaRecyclerAdapter(generateDisciplinas()));
    }

    private ArrayList<Disciplina> generateDisciplinas() {
        ArrayList<Disciplina> recipes = new ArrayList<>();
        recipes.add(new Disciplina("Banco de Dados 1", "Altigran", "Lajos Neto", "CB01", "2015/2"));
        recipes.add(new Disciplina("Projeto Final 2", "Celso", "Lajos Neto", "FT05", "2015/2"));
        recipes.add(new Disciplina("Tópicos em RI", "David", "Lajos Neto", "CB01", "2015/2"));
        recipes.add(new Disciplina("Sis. Colab.", "Bruno", "Lajos Neto", "CB01", "2015/2"));
        recipes.add(new Disciplina("Lab. Controle", "Valdir Sampaio", "Lajos Neto", "FTL09", "2015/2"));
        return recipes;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
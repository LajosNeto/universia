package code.universia;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;

import java.util.ArrayList;
import java.util.List;

// COLOCAR NO LUGAR DE PALETTEVIEWHOLDER O NOME DA VIEWHOLDER QUE O RECYCLER VAI TRABALHAR
public class DisciplinaRecyclerAdapter extends RecyclerView.Adapter<DisciplinaViewHolder>{

    // DECLARAR UMA LISTA DOS OBJETOS
    private List<Disciplina> disciplinas;

    // PASSAR A LISTA DE OBJETOS COMO PARAMETRO PARA O RECYCLER ADAPTER
    public DisciplinaRecyclerAdapter(List<Disciplina> palettes){
        this.disciplinas = new ArrayList<Disciplina>();
        this.disciplinas.addAll(palettes);
    }

    // INFLAR O XML DO CARD
    @Override
    public DisciplinaViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.disciplina_card, viewGroup, false);

        return new DisciplinaViewHolder(itemView);
    }

    // CAMINHAR DENTRO DA LISTA DE OBJETOS E ATRIBUIR OS VALORES DE CADA CAMPO DO CARD
    @Override
    public void onBindViewHolder(DisciplinaViewHolder disciplinaViewHolder, int i) {
        Disciplina disciplina = disciplinas.get(i);
        disciplinaViewHolder.disciplinaNome.setText(disciplina.getName());
        disciplinaViewHolder.disciplinaCodigo.setText(disciplina.getCodigo());
        disciplinaViewHolder.disciplinaCriador.setText(disciplina.getCriador());
        disciplinaViewHolder.disciplinaProfessor.setText(disciplina.getProfessor());
        disciplinaViewHolder.disciplinaPeriodo.setText(disciplina.getPeriodo());
    }

    @Override
    public int getItemCount() {
        return disciplinas.size();
    }
}
package code.universia;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


public class DisciplinaViewHolder extends RecyclerView.ViewHolder {
    // DECLARAR TODOS OS COMPONENTES PRESENTES DENTOR DO CARD_VIEW, QUE RECBERÃO OS CONTEÚDOS DO BANCO DE DADOS
    protected TextView disciplinaNome;
    protected TextView disciplinaCodigo;
    protected TextView disciplinaCriador;
    protected TextView disciplinaProfessor;
    protected TextView disciplinaPeriodo;
    protected CardView card;

    public DisciplinaViewHolder(View itemView) {
        // INSTANCIAR SEMPRE O SUPER COM ITEMVIEW
        super(itemView);
        // INSTANCIAR OS COMPONENTES
        disciplinaNome = (TextView) itemView.findViewById(R.id.nome);
        disciplinaCodigo = (TextView) itemView.findViewById(R.id.codigo);
        disciplinaCriador = (TextView) itemView.findViewById(R.id.criadaPor);
        disciplinaProfessor = (TextView) itemView.findViewById(R.id.professor);
        disciplinaPeriodo = (TextView) itemView.findViewById(R.id.periodo);
        // INSTANCIAR SEMPRE UM CARDVIEW
        card = (CardView) itemView;
    }
}

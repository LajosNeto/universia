package code.universia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setBackgroundDrawableResource(R.drawable.not_t);

        final Button connect_button = (Button) findViewById(R.id.connect_button);
        connect_button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                final EditText email = (EditText) findViewById(R.id.editText);
                final EditText pass = (EditText) findViewById(R.id.editText2);
                String user_email = email.getText().toString();
                String user_pass = pass.getText().toString();

                if(user_email.equals("lajosneto@gmail.com") && user_pass.equals("123456")){

                    Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
}

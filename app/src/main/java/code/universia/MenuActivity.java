package code.universia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu2);
//        getWindow().setBackgroundDrawableResource(R.drawable.board_bg);

//        ImageButton profile_button = (ImageButton) findViewById(R.id.profile_button);
        Button disciplina_button = (Button) findViewById(R.id.disciplina_button);
        disciplina_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, DisciplinaActivity.class);
                startActivity(intent);
            }
        });
    }
}
